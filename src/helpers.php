<?php

declare(strict_types=1);

use Apro\Upload\Storage;
use Apro\Upload\Storage\S3Storage;

if (! function_exists('storage')) {
    /**
     * @throws Exception
     */
    function storage(string $storage = 's3'): Storage
    {
        switch ($storage) {
            case 's3':
                return new S3Storage([
                    'url'                     => '',
                    'endpoint'                => '',
                    'bucket'                  => '',
                    'region'                  => '',
                    'secret'                  => '',
                    'key'                     => '',
                    'directory'               => '',
                    'use_path_style_endpoint' => '',
                ]);
            default:
                throw new Exception('找不到可用的儲存驅動');
        }
    }
}
