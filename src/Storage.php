<?php

declare(strict_types=1);

namespace Apro\Upload;

interface Storage
{
    public function put(string $path, string $contents, array $options = []): string;

    public function url(string $path): string;
}
