<?php

declare(strict_types=1);

namespace Apro\Upload\Storage;

use Apro\Upload\Storage;
use Aws\S3\S3Client;

class S3Storage implements Storage
{
    protected $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function put(string $path, string $contents, array $options = []): string
    {
        $filepath = $this->config['directory'] . '/' . $path;
        $this->connection()->putObject([
            'Bucket' => $this->config['bucket'],
            'Key'    => $filepath,
            'Body'   => fopen($this->config['file'], 'r'),
        ]);

        return $filepath;
    }

    public function url(string $path): string
    {
        return $this->config['url'] . '/' . $this->config['directory'] . '/' . $path;
    }

    protected function connection(): S3Client
    {
        return new S3Client([
            'version'     => 'latest',
            'region'      => $this->config['region'],
            'credentials' => [
                'key'    => $this->config['key'],
                'secret' => $this->config['secret'],
            ],
            'endpoint'                => $this->config['endpoint'],
            'use_path_style_endpoint' => $this->config['use_path_style_endpoint'],
        ]);
    }
}
